import os
import time
import random
import logging
from argparse import ArgumentParser, RawTextHelpFormatter
from math import sqrt
from datetime import datetime

import psycopg2
from psycopg2.errors import SerializationFailure

def create_table(conn):
	with conn.cursor() as cur:
		cur.execute(
				"CREATE TABLE IF NOT EXISTS wildlife (id INT PRIMARY KEY, long FLOAT, lat FLOAT, img STRING, time INT, phone STRING, species STRING)"
				)
		print("Created table")
	conn.commit()

def nuke_db():
	global conn
	with conn.cursor() as cur:
		cur.execute("DROP TABLE wildlife")
	conn.commit()

from uuid import uuid4
def new_id():
	return int(uuid4().time_low)

def add_sighting(coordinates, phone_number, species, img):
	global conn
	run_operation(conn, lambda conn: _add_sighting(conn, coordinates[0], coordinates[1], phone_number, species, img))

def _add_sighting(conn, lat, long, phone, species, img):
	id = new_id()
	with conn.cursor() as cur:
		query = "INSERT INTO wildlife(id, long, lat, img, time, phone, species) VALUES (%s,%s,%s,%s,%s,%s,%s)"
		print(query)
		cur.execute(query, (id, long, lat, img, int(time.time()), phone, species))
	conn.commit()

from math import pi, sin, cos, atan2
RADIUS_OF_EARTH = 6378.1 # in kilometers
#a and b are tuples with (lat, long) in degrees
def earth_dist(a, b): # returns distance in kilometers
	#convert to radians
	a = (a[0]*(pi/180), a[1]*(pi/180))
	b = (b[0]*(pi/180), b[1]*(pi/180))
	#some fancy math shit i copied from here: https://www.movable-type.co.uk/scripts/latlong.html
	d_lat = (b[0]-a[0])
	d_long = (b[1]-a[1])
	a = sin(d_lat/2) * sin(d_lat/2) + cos(a[0]) * cos(b[0]) * sin(d_long/2) * sin(d_long/2)
	c = 2 * atan2(sqrt(a), sqrt(1-a))
	return RADIUS_OF_EARTH * c

MAX_DIST = 20 # in kilometers
def get_nearby(coordinates, n=3):
	global conn
	return run_operation(conn, lambda conn: _get_nearby(conn, coordinates[0], coordinates[1], MAX_DIST, n))

def _get_nearby(conn, lat, long, max_dist, n):
	with conn.cursor() as cur:
		cur.execute("SELECT time, lat, long, img, species FROM wildlife")
		rows = cur.fetchall()
		conn.commit()
		min_dist = max_dist+1
		nearby = []
		for r in range(len(rows)):
			dist = earth_dist((rows[r][1], rows[r][2]), (lat, long))
			if(dist < max_dist):
				nearby.append(rows[r])
		if(len(rows) == 0):
			return []
		ret = []
		for i in range(n):
			recent_ind = -1
			recent_time = 0
			recent = rows[0]
			for r in range(len(nearby)):
				if(nearby[r][0] > recent_time):
					recent_time = nearby[r][0]
					recent = nearby[r]
					recent_ind = r
			if(recent_ind != -1):
				ret.append((datetime.fromtimestamp(recent[0]),(recent[1],recent[2]), recent[3], recent[4]))
				nearby.pop(recent_ind)
			else:
				break
		return ret

def get_species(coordinates, species, n=3):
	l = get_nearby(coordinates, n)
	# TODO: fuzzy searching
	filtered = filter(lambda it: it[3].lower() == species.lower(), l)
	return filtered



def run_operation(conn, op, max_retries=3):
	"""
	Execute the operation *op(conn)* retrying serialization failure.
	If the database returns an error asking to retry the transaction, retry it
	*max_retries* times before giving up (and propagate it).
	"""
	# leaving this block the operation will commit or rollback
	# (if leaving with an exception)
	with conn:
		for retry in range(1, max_retries + 1):
			try:
				ret =  op(conn)
				# If we reach this point, we were able to commit, so we break
				# from the retry loop.
				return ret
			except SerializationFailure as e:
				# This is a retry error, so we roll back the current
				# transaction and sleep for a bit before retrying. The
				# sleep time increases for each failed transaction.
				logging.debug("got error: %s", e)
				conn.rollback()
				logging.debug("EXECUTE SERIALIZATION_FAILURE BRANCH")
				sleep_ms = (2 ** retry) * 0.1 * (random.random() + 0.5)
				logging.debug("Sleeping %s seconds", sleep_ms)
				time.sleep(sleep_ms)
			except psycopg2.Error as e:
				logging.debug("got error: %s", e)
				logging.debug("EXECUTE NON-SERIALIZATION_FAILURE BRANCH")
				raise e
		raise ValueError(f"operation did not succeed after {max_retries} retries")

def close():
	conn.close()

def connect():
	global conn
	conn = psycopg2.connect(os.getenv("DATABASE_URL"))
	run_operation(conn, lambda conn: create_table(conn))

import util
#example usage
if(__name__ == "__main__"):
	connect()
	
	#coordinates phone_number species img
	#add_sighting((40, 0), "19999999999", "Human1", "url")

	#takes coordinates (lat, long) and returns a tuple containing (datetime.datetime, (lat, long), string image_url, string species)
	a = get_nearby((40, 0), 1)[0][0].timestamp()
	b = time.time()
	print([a, b])
	print(util.seconds_to_string(a-b))
	
	#nuke_db()# WARNING: THIS WILL NUKE EVERYTHING
	close()
