from pip._vendor import requests


def coords_from_exif(imageUrl):
    import os
    import PIL.Image
    import PIL.ExifTags
    response = requests.get(imageUrl)

    image = PIL.Image.open(response.content)
    # Creates a dict with the key of the appropriate ExifTag, and a value of the actual EXIF data
    exif = {
        PIL.ExifTags.TAGS[k]: v
        for k, v in image._getexif().items()
        if k in PIL.ExifTags.TAGS
    }
    # Extracts the GPS section of the image's EXIF data.
    GPSInfo = exif['GPSInfo']
    if GPSInfo not in exif:
        return("NOOOO")
    print(GPSInfo)
    # Extracts the latitude and longitude (which come in three numbers - [degrees], [minutes], [seconds]
    N_degrees_secs = GPSInfo[2]
    print(GPSInfo[2])
    W_degrees_secs = GPSInfo[4]
    # Converts the tuples into the more standard single-value format
    print(W_degrees_secs)
    N = int(N_degrees_secs[0]) + int(N_degrees_secs[1]) / 60 + int(N_degrees_secs[2]) / 3600
    W = -1 * (int(W_degrees_secs[0]) + int(W_degrees_secs[1]) / 60 + int(W_degrees_secs[2]) / 3600)
    return (N, W)

coords_from_exif("https://s3-external-1.amazonaws.com/media.twiliocdn.com/AC1e75fb0a77f354ab813cc7cf28121abf/cc75e7d92bd5a9a630e1e81f83e025db")