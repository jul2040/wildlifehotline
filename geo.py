from geopy.geocoders import Nominatim


def get_coordinates(address):
	geolocator = Nominatim(user_agent="Cool ppl")
	location = geolocator.geocode(address)
	if(location is None):
		return None
	return ((location.latitude, location.longitude),location.address)
