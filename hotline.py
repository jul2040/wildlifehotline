#!/usr/bin/python3
from threading import Timer

from flask import Flask, request
from twilio.twiml.messaging_response import MessagingResponse

import db
import geo
import util
from time import time
from datetime import datetime

# schema is:
# key: phone number
# value: dict {1: (species, image), 2: undo timer}
conversations = {}

app = Flask(__name__)


@app.route("/sms", methods=['GET', 'POST'])
def sms_reply():
	print("debug!")
	# Start our TwiML response
	resp = MessagingResponse()

	body = request.values.get('Body', None)
	num = request.values.get("From")
	num_media = int(request.values.get("NumMedia"))

	if num in conversations:
		if body.lower() in ["undo", "cancel"]:
			if 2 in conversations[num]:
				conversations[num][2].cancel()  # cancels the submission
				conversations.pop(num)
				resp.message("cancelled submission! please submit again to correct.")
				return str(resp)
		elif 2 in conversations[num]:
			conversations.pop(num)  # their undo timer is still running, and we just got a message that isnt "undo"

	if body.lower() in ["tutorial", "guide", "help"]:
		resp.message('To send a sighting, reply with an image of the sighting, and the format `Species "x" Location "x"`.')
		resp.message('For example, if you wanted to submit a bee sighting you would say Species "bee" location "NJIT".')
		resp.message("To search for a sighting, either type in the location for all sightings or use syntax for submitting without attaching image.")
		return str(resp)
	
	if num not in conversations and num_media == 0:
		list_body = body.lower().split('"')
		(species, location, sightings) = (None, None, 3)
		list_stripped = []
		print(list_body)
		for item in list_body:
			itemStrip = item.strip()
			n = list_body.index(item)
			if itemStrip == "species":
				species = list_body[n + 1].strip(" ")
			if itemStrip == "location":
				location = list_body[n + 1].strip(" ")
			if itemStrip == "sightings":
				sightings = int(list_body[n + 1].strip(" "))
			list_stripped.append(itemStrip)
		if "species" not in list_stripped and "location" not in list_stripped and "sightings" not in list_stripped:
			numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]
			# removes number from body to use later on when finding nearest sightings. if no number then defaults to 3
			number_check = body[-2:]
			if number_check[0] == " " and int(number_check[1]) in numbers:
				sightings = int(number_check[1])
				location = body[:-2]
			else:
				sightings = 3
				location = body

		coords_location = geo.get_coordinates(location)
		if coords_location is None:
			resp.message("Oops! I couldn't find that place. Please rephrase your location.")
			return str(resp)
			pass

		if species is None:
			list_of_sightings = db.get_nearby(coords_location[0], sightings)
		else:
			list_of_sightings = list(db.get_species(coords_location[0], species, sightings))


		if len(list_of_sightings) == 0:
			resp.message("No sightings found!")
			return str(resp)

		for item in list_of_sightings:
			resp.message(
				f"Found nearby {item[3]} sighting\n{util.seconds_to_string(time() - (item[0].timestamp()))} ago\n{round(db.earth_dist(item[1], coords_location[0]), 2)} km away.").media(
				item[2])
		return str(resp)

	if num not in conversations and num_media != 0:  # if an image is attached to the text:
		list_body = body.lower().split('"')
		species = None
		location = None
		list_stripped = []
		for item in list_body:
			itemStrip = item.strip()
			n = list_body.index(item)
			if itemStrip == "species":
				species = list_body[n+1].strip(" ")
			if itemStrip == "location":
				location = list_body[n+1].strip(" ")
			list_stripped.append(itemStrip)
		if "species" not in list_stripped and "location" not in list_stripped:
			species = body
		if species is None:
			species = "Unknown"
		media_url = request.values.get("MediaUrl0")
		conversations[num] = {1: (species, media_url)}
		if location is None:
			resp.message(f"Please enter your current location to submit.")
		else:  # Runs location-finding code here if a location is given.
			(coords, text) = geo.get_coordinates(location) or (None, None)  # TODO: nicer error handling
			if text is None:
				resp.message("Error: unknown location. Closing request.")
			else:
				resp.message(f"Submitted. Species: {conversations[num][1][0]}\nLocation: {text}\n\nSend \"undo\" to undo this submission")
			t = Timer(15, end_submission, (num, coords, conversations[num][1][0],conversations[num][1][1]))  # will execute in 15 seconds unless cancelled
			conversations[num][2] = t  # keep track of timer so it can be cancelled
			t.start()  # start the timer
		return str(resp)

	if 2 not in conversations[num]:  # stage 2: give location
		(coords, text) = geo.get_coordinates(body) or (None, None)  # TODO: nicer error handling
		if text is None:
			resp.message("Error: unknown location. Closing request.")
		else:
			resp.message(f"Submitted. Species: {conversations[num][1][0]}\nLocation: {text}\n\nSend \"undo\" to undo this submission")
		t = Timer(15, end_submission, (num, coords, conversations[num][1][0], conversations[num][1][1]))  # will execute in 30 seconds unless cancelled
		conversations[num][2] = t  # keep track of timer so it can be cancelled
		t.start()  # start the timer
		return str(resp)
	return str(resp)

def end_submission(number, coordinates, species, img):
	if(number in conversations):
		conversations.pop(number)
	db.add_sighting(coordinates, number, species, img)

if __name__ == "__main__":
	db.connect()
	app.run(debug=True)
