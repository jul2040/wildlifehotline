from math import floor
#takes a number of seconds and returns a human readable string
# example: "5 days "
def seconds_to_string(seconds):
	print(seconds)
	seconds = abs(seconds)
	minutes = floor(seconds/60)
	seconds = seconds%60
	hours = floor(minutes/60)
	minutes = minutes%60
	days = floor(hours/24)
	hours = hours%24
	seconds = round(seconds)
	minutes = round(minutes)
	hours = round(hours)
	days = round(days)
	print([days, hours, minutes, seconds])
	string = ""
	if(days > 0):
		if(days == 1):
			string = f"{days} day"
		else:
			string = f"{days} days"
	if(hours > 0):
		if(hours == 1):
			string = string + f" {hours} hour"
		else:
			string = string + f" {hours} hours"
	if(minutes > 0 and days == 0):
		if(minutes == 1):
			string = string + f" {minutes} minute"
		else:
			string = string + f" {minutes} minutes"
	if(seconds >= 0 and hours == 0):
		if(seconds == 1):
			string = string + f" {seconds} second"
		else:
			string = string + f" {seconds} seconds"
	return string

